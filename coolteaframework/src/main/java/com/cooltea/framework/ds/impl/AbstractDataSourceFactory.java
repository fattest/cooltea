package com.cooltea.framework.ds.impl;

import com.cooltea.framework.ConfigConstant;
import com.cooltea.framework.core.ConfigHelper;
import com.cooltea.framework.ds.DataSourceFactory;

import javax.sql.DataSource;

/**
 * 抽象数据源工厂
 *
 */
public abstract class AbstractDataSourceFactory<T extends DataSource> implements DataSourceFactory {

    protected final String driver = ConfigHelper.getString(ConfigConstant.DB_DRIVER);
    protected final String url = ConfigHelper.getString(ConfigConstant.DB_URL);
    protected final String username = ConfigHelper.getString(ConfigConstant.DB_USER);
    protected final String password = ConfigHelper.getString(ConfigConstant.DB_PASSWORD);

    @Override
    public final T getDataSource() {
        // 创建数据源对象
        T ds = createDataSource();
        // 设置基础属性
        setDriver(ds, driver);
        setUrl(ds, url);
        setUsername(ds, username);
        setPassword(ds, password);
        // 设置高级属性
        setAdvancedConfig(ds);
        return ds;
    }

    public abstract T createDataSource();

    public abstract void setDriver(T ds, String driver);

    public abstract void setUrl(T ds, String url);

    public abstract void setUsername(T ds, String username);

    public abstract void setPassword(T ds, String password);

    public abstract void setAdvancedConfig(T ds);
}
