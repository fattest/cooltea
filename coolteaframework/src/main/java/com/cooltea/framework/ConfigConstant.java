package com.cooltea.framework;

public interface ConfigConstant {

    String BasePackage = "smart.framework.app.base_package";

    /**
     * ClassScanner
     */
    String CLASS_SCANNER = "smart.framework.custom.class_scanner";

    /**
     * DataSourceFactory
     */
    String DS_FACTORY = "smart.framework.custom.ds_factory";

    /**
     * DataAccessor
     */
    String DATA_ACCESSOR = "smart.framework.custom.data_accessor";

    /**
     * db
     */
    String DB_TYPE = "smart.framework.jdbc.type";
    String DB_DRIVER = "smart.framework.jdbc.driver";
    String DB_URL = "smart.framework.jdbc.url";
    String DB_USER = "smart.framework.jdbc.username";
    String DB_PASSWORD = "smart.framework.jdbc.password";

    /**
     * web
     */
    String HANDLER_MAPPING = "smart.framework.custom.handler_mapping";
    String HANDLER_INVOKER = "smart.framework.custom.handler_invoker";
    String HANDLER_EXCEPTION_RESOLVER = "smart.framework.custom.handler_exception_resolver";
    String VIEW_RESOLVER = "smart.framework.custom.view_resolver";

}
