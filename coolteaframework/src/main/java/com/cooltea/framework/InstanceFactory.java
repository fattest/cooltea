package com.cooltea.framework;

import com.cooltea.framework.core.classscanner.ClassScanner;
import com.cooltea.framework.core.ConfigHelper;
import com.cooltea.framework.core.classscanner.impl.DefaultClassScanner;
import com.cooltea.framework.dao.DataAccessor;
import com.cooltea.framework.dao.impl.DefaultDataAccessor;
import com.cooltea.framework.ds.DataSourceFactory;
import com.cooltea.framework.ds.impl.DefaultDataSourceFactory;
import com.cooltea.framework.mvc.HandlerExceptionResolver;
import com.cooltea.framework.mvc.HandlerInvoker;
import com.cooltea.framework.mvc.HandlerMapping;
import com.cooltea.framework.mvc.ViewResolver;
import com.cooltea.framework.mvc.impl.DefaultHandlerExceptionResolver;
import com.cooltea.framework.mvc.impl.DefaultHandlerInvoker;
import com.cooltea.framework.mvc.impl.DefaultHandlerMapping;
import com.cooltea.framework.mvc.impl.DefaultViewResolver;
import com.cooltea.framework.util.ObjectUtil;
import com.cooltea.framework.util.StringUtil;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 实例工厂
 *
 */
public class InstanceFactory {

    /**
     * 用于缓存对应的实例
     */
    private static final Map<String, Object> cache = new ConcurrentHashMap<String, Object>();

    /**
     * ClassScanner
     */
    private static final String CLASS_SCANNER = ConfigConstant.CLASS_SCANNER;

    /**
     * DataSourceFactory
     */
    private static final String DS_FACTORY = ConfigConstant.DS_FACTORY;

    /**
     * DataAccessor
     */
    private static final String DATA_ACCESSOR = ConfigConstant.DATA_ACCESSOR;

    /**
     * HandlerMapping
     */
    private static final String HANDLER_MAPPING = ConfigConstant.HANDLER_MAPPING;

    /**
     * HandlerInvoker
     */
    private static final String HANDLER_INVOKER = ConfigConstant.HANDLER_INVOKER;

    /**
     * HandlerExceptionResolver
     */
    private static final String HANDLER_EXCEPTION_RESOLVER = ConfigConstant.HANDLER_EXCEPTION_RESOLVER;

    /**
     * ViewResolver
     */
    private static final String VIEW_RESOLVER = ConfigConstant.VIEW_RESOLVER;

    /**
     * 获取 ClassScanner
     */
    public static ClassScanner getClassScanner() {
        return getInstance(CLASS_SCANNER, DefaultClassScanner.class);
    }

    /**
     * 获取 DataSourceFactory
     */
    public static DataSourceFactory getDataSourceFactory() {
        return getInstance(DS_FACTORY, DefaultDataSourceFactory.class);
    }

    /**
     * 获取 DataAccessor
     */
    public static DataAccessor getDataAccessor() {
        return getInstance(DATA_ACCESSOR, DefaultDataAccessor.class);
    }

    /**
     * 获取 HandlerMapping
     */
    public static HandlerMapping getHandlerMapping() {
        return getInstance(HANDLER_MAPPING, DefaultHandlerMapping.class);
    }

    /**
     * 获取 HandlerInvoker
     */
    public static HandlerInvoker getHandlerInvoker() {
        return getInstance(HANDLER_INVOKER, DefaultHandlerInvoker.class);
    }

    /**
     * 获取 HandlerExceptionResolver
     */
    public static HandlerExceptionResolver getHandlerExceptionResolver() {
        return getInstance(HANDLER_EXCEPTION_RESOLVER, DefaultHandlerExceptionResolver.class);
    }

    /**
     * 获取 ViewResolver
     */
    public static ViewResolver getViewResolver() {
        return getInstance(VIEW_RESOLVER, DefaultViewResolver.class);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getInstance(String cacheKey, Class<T> defaultImplClass) {
        // 若缓存中存在对应的实例，则返回该实例
        if (cache.containsKey(cacheKey)) {
            return (T) cache.get(cacheKey);
        }
        // 从配置文件中获取相应的接口实现类配置
        String implClassName = ConfigHelper.getString(cacheKey);
        // 若实现类配置不存在，则使用默认实现类
        if (StringUtil.isEmpty(implClassName)) {
            implClassName = defaultImplClass.getName();
        }
        // 通过反射创建该实现类对应的实例
        T instance = ObjectUtil.newInstance(implClassName);
        // 若该实例不为空，则将其放入缓存
        if (instance != null) {
            cache.put(cacheKey, instance);
        }
        // 返回该实例
        return instance;
    }
}
