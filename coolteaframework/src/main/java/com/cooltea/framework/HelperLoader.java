package com.cooltea.framework;

import com.cooltea.framework.aop.AopHelper;
import com.cooltea.framework.dao.DatabaseHelper;
import com.cooltea.framework.ioc.BeanHelper;
import com.cooltea.framework.ioc.IocHelper;
import com.cooltea.framework.mvc.ActionHelper;
import com.cooltea.framework.orm.EntityHelper;
import com.cooltea.framework.plugin.PluginHelper;
import com.cooltea.framework.util.ClassUtil;

/**
 * 加载相应的 Helper 类
 *
 */
public final class HelperLoader {

    public static void init() {
        // 定义需要加载的 Helper 类
        Class<?>[] classList = {
            DatabaseHelper.class,
            EntityHelper.class,
            BeanHelper.class,
            ActionHelper.class,
            AopHelper.class,
            IocHelper.class,
            PluginHelper.class,
        };
        // 按照顺序加载类
        for (Class<?> cls : classList) {
            ClassUtil.loadClass(cls.getName());
        }
    }
}
