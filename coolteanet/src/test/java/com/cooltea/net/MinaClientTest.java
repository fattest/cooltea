package com.cooltea.net;

import com.cooltea.net.mina.MessageCodecFactory;
import com.cooltea.net.mina.MinaEncoder;
import com.cooltea.net.mina.MinaHandler;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.FilterEvent;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketConnector;
import org.omg.PortableInterceptor.ServerRequestInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class MinaClientTest {
    static Logger logger = LoggerFactory.getLogger("Info");
    public static void main(String args[]) throws InterruptedException {
        NioSocketConnector connector = new NioSocketConnector(1);
        connector.getFilterChain().addLast("encoder", new ProtocolCodecFilter(new MessageCodecFactory()));
        connector.setHandler(new IoHandlerAdapter() {
            @Override
            public void messageReceived(IoSession session, Object message) throws Exception {
                if (message instanceof CoolTeaProto) {
                    CoolTeaProto ctp = (CoolTeaProto)message;
                    System.out.println("receive===" + new String(ctp.getData()));
                }
            }

            @Override
            public void messageSent(IoSession session, Object message) throws Exception {
                System.out.println("session send " + message.getClass().getName());
            }
        });
        ConnectFuture cf = connector.connect(new InetSocketAddress("localhost", 8080));
        String str = new String("中国人");
        CoolTeaProto ctp = new CoolTeaProto(132, str.getBytes());
        cf.await();
        cf.getSession().write(ctp);

        System.out.println("发送完成");
    }
}
