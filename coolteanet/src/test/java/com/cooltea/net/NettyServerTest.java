package com.cooltea.net;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.NetParam;
import com.cooltea.net.NetServer;
import com.cooltea.net.Session.CoolTeaSession;
import com.cooltea.net.callback.CoolTeaIoEventCallback;
import com.cooltea.net.constant.CoolTeaIoFramework;

public class NettyServerTest {
    public  static void main(String args[]) throws Exception {
        NetParam netParam = new NetParam();
        netParam.setTcpPort(8080);
        netParam.setFramework(CoolTeaIoFramework.Netty);
        netParam.setCallback(new MyNettyHandler());
        netParam.setBigEndian(true);
        NetServer.getInstance().init(netParam);
        NetServer.getInstance().start();
    }

    static class MyNettyHandler extends CoolTeaIoEventCallback {

        @Override
        public void onDataReceive(CoolTeaProto coolTeaProto) {
            System.out.println("receive data===="+new String(coolTeaProto.getData()));
        }

        @Override
        public void sessionClose(CoolTeaSession coolTeaSession) {
            System.out.println("client close==============");
        }
    }
}
