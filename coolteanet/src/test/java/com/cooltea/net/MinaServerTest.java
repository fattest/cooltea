package com.cooltea.net;

import com.cooltea.net.Session.CoolTeaSession;
import com.cooltea.net.callback.CoolTeaIoEventCallback;
import com.cooltea.net.constant.CoolTeaIoFramework;

public class MinaServerTest {
    public  static void main(String args[]) throws Exception {
        NetParam netParam = new NetParam();
        netParam.setTcpPort(8080);
        netParam.setFramework(CoolTeaIoFramework.Mina);
        netParam.setCallback(new MinaServerTest.MyMinaHandler());
        netParam.setWorkThread(8);
        netParam.setBigEndian(true);
        NetServer.getInstance().init(netParam);
        NetServer.getInstance().start();
    }

    static class MyMinaHandler extends CoolTeaIoEventCallback {

        @Override
        public void sessionOpen(CoolTeaSession coolTeaSession) {
            System.out.println("session open");
        }
        @Override
        public void onDataReceive(CoolTeaProto coolTeaProto) {
            System.out.println("mina receive data===="+new String(coolTeaProto.getData()));
        }

        @Override
        public void sessionClose(CoolTeaSession coolTeaSession) {
            System.out.println("client close==============");
        }
    }
}
