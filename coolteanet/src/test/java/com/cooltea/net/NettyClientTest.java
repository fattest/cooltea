package com.cooltea.net;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.netty.NettyDecoder;
import com.cooltea.net.netty.NettyEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyClientTest {
    static Logger logger = LoggerFactory.getLogger("Info");
    public static void main(String args[]) throws InterruptedException {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.group(new NioEventLoopGroup(1));
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.handler(new ChannelInitializer<SocketChannel>(){

            @Override
            protected void initChannel(SocketChannel channel) throws Exception {
                channel.pipeline().addLast("encoder", new NettyEncoder());
                channel.pipeline().addLast("decoder", new NettyDecoder());
            }
        });

        ChannelFuture channelFuture = bootstrap.connect("localhost", 8080).sync();
        String str = "abc";
        CoolTeaProto cp = new CoolTeaProto(123, str.getBytes());
        channelFuture.channel().writeAndFlush(cp);
        logger.info("send data over");
    }

}
