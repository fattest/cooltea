package com.cooltea.net;

import com.cooltea.net.Session.CoolTeaSession;
import io.netty.buffer.ByteBuf;

public class CoolTeaProto {
    /**
     * 协议头
     */
    private int protocolType;
    /**
     * 字节.
     */
    private byte[] data;
    /**
     * 写入
     */
    private CoolTeaSession session;

    public CoolTeaProto(int protocolType, byte[] bytes) {
        this.protocolType = protocolType;
        this.data = bytes;
    }

    public byte[] getData() {
        return data;
    }

    public int getProtocolType() {
        return protocolType;
    }

    public boolean write() {
        if (session != null) {
            session.write(this);
            return true;
        }
        return false;
    }

    public CoolTeaSession getSession() {
        return session;
    }

    public void setSession(CoolTeaSession session) {
        this.session = session;
    }
}
