package com.cooltea.net;

import com.cooltea.net.callback.CoolTeaIoEventCallback;
import com.cooltea.net.constant.CoolTeaIoFramework;
import com.cooltea.net.mina.MessageCodecFactory;
import com.cooltea.net.mina.MinaHandler;
import com.cooltea.net.netty.NettyDecoder;
import com.cooltea.net.netty.NettyEncoder;
import com.cooltea.net.netty.NettyIoHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;

public class NetServer {
    Logger logger = LoggerFactory.getLogger(NetServer.class);
    private NetParam netParam;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private IoAcceptor ioAcceptor;
    public static NetServer getInstance() {
        return NetServerHolder.netServer;
    }

    static class NetServerHolder {
        private static NetServer netServer = new NetServer();
    }


    public void init(NetParam netParam) {
        this.netParam = netParam;
    }

    public void start() throws Exception{
        if (netParam.getFramework() == CoolTeaIoFramework.Netty) {
            startNettyServer();
        } else {
            startMinaServer();
        }
    }

    public boolean isBigEndian() {
        if (netParam != null) {
            return netParam.isBigEndian();
        } else {
            return true;
        }

    }

    public CoolTeaIoFramework getFramework() {
        return netParam.getFramework();
    }

    public CoolTeaIoEventCallback getCallback() {
        return netParam.getCallback();
    }

    private boolean startNettyServer() throws InterruptedException {
        //如果是netty
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup(netParam.getWorkThread());
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class) // 说明一个新的Channel如何接收进来的连接
                .option(ChannelOption.SO_BACKLOG, 128) // tcp最大缓存链接个数
                .option(ChannelOption.ALLOCATOR, ByteBufAllocator.DEFAULT)
                .childOption(ChannelOption.SO_KEEPALIVE, true) //保持连接
                .childOption(ChannelOption.SO_SNDBUF, netParam.getSendBuf())
                .childOption(ChannelOption.SO_RCVBUF, netParam.getReceiveBuff())
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast("encoder", new NettyEncoder()); // 这里相当于过滤器，可以配置多个
                        socketChannel.pipeline().addLast("decoder", new NettyDecoder());
                        socketChannel.pipeline().addLast("ioHandler", new NettyIoHandler());
                    }
                });
        ChannelFuture cf = bootstrap.bind(netParam.getTcpPort()).sync();
        logger.info("start netty server");
        return true;
    }

    private boolean startMinaServer () throws IOException {
        ioAcceptor = new NioSocketAcceptor(netParam.getWorkThread());
        ioAcceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new MessageCodecFactory()));
        ioAcceptor.setHandler(new MinaHandler());
        ioAcceptor.getSessionConfig().setReadBufferSize(netParam.getReceiveBuff());
        ioAcceptor.bind( new InetSocketAddress(netParam.getTcpPort()) );
        logger.info("start mina server");
        return true;
    }

    public void stopServer() {
        logger.info("stop net server");
        if (netParam.getFramework() == CoolTeaIoFramework.Netty) {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        } else if (netParam.getFramework() == CoolTeaIoFramework.Mina) {
            ioAcceptor.dispose();
        }
    }

}