package com.cooltea.net;

import com.cooltea.net.callback.CoolTeaIoEventCallback;
import com.cooltea.net.constant.CoolTeaIoFramework;

public class NetParam {
    private boolean isBigEndian = false;
    /**
     * io框架。
     */
    private CoolTeaIoFramework framework;

    /**
     * 回调事件.
     */
    private CoolTeaIoEventCallback callback;
    /**
     * 工作线程数.
     */
    private int workThread;

    /**
     * 绑定端口.
     */
    private int tcpPort;
    /**
     * 接收buff
     */
    private int receiveBuff = 1024 * 16;
    /**
     * 发送buff
     */
    private int sendBuf = 1024 * 16;

    public boolean isBigEndian() {
        return isBigEndian;
    }

    public void setBigEndian(boolean bigEndian) {
        isBigEndian = bigEndian;
    }

    public CoolTeaIoFramework getFramework() {
        return framework;
    }

    public void setFramework(CoolTeaIoFramework framework) {
        this.framework = framework;
    }

    public CoolTeaIoEventCallback getCallback() {
        return callback;
    }

    public void setCallback(CoolTeaIoEventCallback callback) {
        this.callback = callback;
    }

    public int getWorkThread() {
        return workThread;
    }

    public void setWorkThread(int workThread) {
        this.workThread = workThread;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public void setTcpPort(int tcpPort) {
        this.tcpPort = tcpPort;
    }

    public int getReceiveBuff() {
        return receiveBuff;
    }

    public void setReceiveBuff(int receiveBuff) {
        this.receiveBuff = receiveBuff;
    }

    public int getSendBuf() {
        return sendBuf;
    }

    public void setSendBuf(int sendBuf) {
        this.sendBuf = sendBuf;
    }
}
