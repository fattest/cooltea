package com.cooltea.net.mina;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.NetServer;
import com.cooltea.net.constant.NetConst;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import java.nio.ByteOrder;

public class MinaDecoder extends CumulativeProtocolDecoder {
    static int MAX_LENGTH = 16 * 1024;
    @Override
    protected boolean doDecode(IoSession ioSession, IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        if (NetServer.getInstance().isBigEndian()) {
            ioBuffer.order(ByteOrder.BIG_ENDIAN);
        } else {
            ioBuffer.order(ByteOrder.LITTLE_ENDIAN);
        }
        if (ioBuffer.remaining() < NetConst.HEADER_LENGTH) {
            return false;
        }
        ioBuffer.mark();
        int length = ioBuffer.getInt();
        if (length > MAX_LENGTH) {
            ioBuffer.clear();
            return false;
        }
        if (ioBuffer.remaining() < length) {
            ioBuffer.reset();
            return false;
        }
        int protocolType = ioBuffer.getInt();
        byte[] bytes = new byte[length - 4];
        ioBuffer.get(bytes);
        protocolDecoderOutput.write(new CoolTeaProto(protocolType, bytes));

        return true;
    }
}
