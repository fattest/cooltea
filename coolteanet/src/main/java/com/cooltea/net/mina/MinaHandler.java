package com.cooltea.net.mina;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.NetServer;
import com.cooltea.net.Session.CoolTeaSession;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MinaHandler extends IoHandlerAdapter {
    static Logger logger = LoggerFactory.getLogger(IoHandlerAdapter.class);
    private CoolTeaSession session;
    public MinaHandler () {

    }
    @Override
    public void sessionOpened(IoSession ioSession) throws Exception {
        session = new CoolTeaSession();
        session.setIoSession(ioSession);
        NetServer.getInstance().getCallback().sessionOpen(session);
    }

    @Override
    public void sessionClosed(IoSession ioSession) throws Exception {
        NetServer.getInstance().getCallback().sessionClose(session);
    }

    @Override
    public void messageReceived(IoSession ioSession, Object o) throws Exception {
        if (o instanceof CoolTeaProto) {
            CoolTeaProto coolTeaProto = (CoolTeaProto)o;
            coolTeaProto.setSession(session);
            NetServer.getInstance().getCallback().onDataReceive(coolTeaProto);
        }
    }

    @Override
    public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
        logger.error("net error", cause);
    }

}
