package com.cooltea.net.mina;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.NetServer;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import java.nio.ByteOrder;

public class MinaEncoder extends ProtocolEncoderAdapter {
    @Override
    public void encode(IoSession ioSession, Object o, ProtocolEncoderOutput protocolEncoderOutput) throws Exception {
        CoolTeaProto coolTeaProto = (CoolTeaProto)o;
        int length = coolTeaProto.getData().length + 4;
        IoBuffer ioBuffer = IoBuffer.allocate(length + 4);
        if (NetServer.getInstance().isBigEndian()) {
            ioBuffer.order(ByteOrder.BIG_ENDIAN);
        } else {
            ioBuffer.order(ByteOrder.LITTLE_ENDIAN);
        }
        ioBuffer.putInt(length);
        ioBuffer.putInt(coolTeaProto.getProtocolType());
        ioBuffer.put(coolTeaProto.getData());
        ioBuffer.flip();
        protocolEncoderOutput.write(ioBuffer);
    }
}
