package com.cooltea.net.mina;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class MessageCodecFactory implements ProtocolCodecFactory {
    ProtocolEncoder protocolEncoder;
    ProtocolDecoder protocolDecoder;
    public MessageCodecFactory() {
        protocolEncoder = new MinaEncoder();
        protocolDecoder = new MinaDecoder();
    }
    @Override
    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
        return protocolEncoder;
    }

    @Override
    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
        return protocolDecoder;
    }
}
