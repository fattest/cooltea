package com.cooltea.net.util;

import com.cooltea.net.NetServer;
import io.netty.buffer.ByteBuf;

public class NetUtil {
    public static void writeInt(ByteBuf buf, int value) {
        if (NetServer.getInstance().isBigEndian()) {
            buf.writeInt(value);
        } else {
            buf.writeIntLE(value);
        }
    }

    public static int readInt(ByteBuf byteBuf) {
        if (NetServer.getInstance().isBigEndian()) {
            return byteBuf.readInt();
        } else {
            return byteBuf.readIntLE();
        }
    }
}
