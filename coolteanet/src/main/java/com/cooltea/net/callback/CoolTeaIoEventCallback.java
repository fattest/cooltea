package com.cooltea.net.callback;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.Session.CoolTeaSession;

public abstract class CoolTeaIoEventCallback {
    /**
     * session open
     * @param coolTeaSession
     */
    public  void sessionOpen(CoolTeaSession coolTeaSession) {
        // do nothing;
    }

    /**
     * 数据事件
     * @param coolTeaProto
     */
    public abstract void onDataReceive(CoolTeaProto coolTeaProto);

    /**
     * session close
     * @param coolTeaSession
     */
    public abstract  void sessionClose(CoolTeaSession coolTeaSession);
}
