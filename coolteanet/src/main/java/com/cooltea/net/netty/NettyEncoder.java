package com.cooltea.net.netty;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.NetServer;
import com.cooltea.net.util.NetUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class NettyEncoder extends MessageToByteEncoder<CoolTeaProto> {
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, CoolTeaProto coolTeaProto, ByteBuf byteBuf) throws Exception {
        int length = coolTeaProto.getData().length + 4 ;
        NetUtil.writeInt(byteBuf, length);
        NetUtil.writeInt(byteBuf, coolTeaProto.getProtocolType());
        byteBuf.writeBytes(coolTeaProto.getData());
    }
}
