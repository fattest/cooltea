package com.cooltea.net.netty;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.constant.NetConst;
import com.cooltea.net.util.NetUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class NettyDecoder extends ByteToMessageDecoder{
    static int MAX_LENGTH = 16 * 1024;
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        if (byteBuf.readableBytes() < NetConst.HEADER_LENGTH) {
            return;
        }
        byteBuf.markReaderIndex();
        int length = NetUtil.readInt(byteBuf);
        if (length > MAX_LENGTH) {
            byteBuf.clear();
            return;
        }
        if (byteBuf.readableBytes() < length) {
            byteBuf.resetReaderIndex();
            return;
        }
        int protocolType = NetUtil.readInt(byteBuf);
        byte[] bytes = new byte[length - 4];
        byteBuf.readBytes(bytes);
        list.add(new CoolTeaProto(protocolType, bytes));
    }
}
