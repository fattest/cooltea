package com.cooltea.net.netty;

import com.cooltea.net.CoolTeaProto;
import com.cooltea.net.NetServer;
import com.cooltea.net.Session.CoolTeaSession;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyIoHandler extends SimpleChannelInboundHandler<CoolTeaProto> {
    static Logger logger = LoggerFactory.getLogger(NettyIoHandler.class);
    private CoolTeaSession session;
    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, CoolTeaProto coolTeaProto) throws Exception {
        coolTeaProto.setSession(session);
        NetServer.getInstance().getCallback().onDataReceive(coolTeaProto);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        session = new CoolTeaSession();
        session.setNettyChannel(ctx.channel());
        ctx.fireChannelActive();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        NetServer.getInstance().getCallback().sessionClose(session);
        this.session = null;
        ctx.fireChannelInactive();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("network exception", cause);
    }
}
