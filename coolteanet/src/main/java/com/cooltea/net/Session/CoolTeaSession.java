package com.cooltea.net.Session;

import com.cooltea.net.CoolTeaProto;
import io.netty.channel.Channel;
import org.apache.mina.core.session.IoSession;

import java.util.HashMap;
import java.util.Map;

public class CoolTeaSession {
    /**
     * 如果是使用netty实现的话此处不为空
     */
    private Channel nettyChannel;
    /**
     * ioSession
     */
    private IoSession ioSession;
    /**
     * 用于存储一些属性
     */
    public Map<Object, Object> attributeMap = new HashMap<>();

    public boolean isOpen() {
        if (nettyChannel != null) {
            return nettyChannel.isOpen();
        }

        if (ioSession != null) {
            return ioSession.isActive();
        }

        return false;
    }

    public void write(CoolTeaProto proto) {
        if (nettyChannel != null && nettyChannel.isActive()) {
            nettyChannel.write(proto);
        }

        if (ioSession != null && ioSession.isActive()) {
            ioSession.write(proto);
        }
    }

    public void setAttribute(Object key, Object value) {
        attributeMap.put(key, value);
    }

    public Object getAttribute(Object key) {
        return attributeMap.get(key);
    }

    public Channel getNettyChannel() {
        return nettyChannel;
    }

    public void setNettyChannel(Channel nettyChannel) {
        this.nettyChannel = nettyChannel;
    }

    public IoSession getIoSession() {
        return ioSession;
    }

    public void setIoSession(IoSession ioSession) {
        this.ioSession = ioSession;
    }
}
